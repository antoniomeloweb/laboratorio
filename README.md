
** Projeto: API para manuten��o de laborat�rios e exames **
O aplicativo desenvolvido utiliza as seguintes tecnologias:

1. Spring Boot 
2. Spring MVC
3. Spring Data
4. Swagger Api documentation
5. H2 Database


---


Ao rodar o aplicativo atrav�s da execu��o da classe:

## br.com.dasa.teste.laboratorio.LaboratorioApplication.java

## API REST

Para acessar a documenta��o da API, acesse o endere�o:

http://localhost:9000/swagger-ui.html

```

IMPORTANTE:

Para facilitar os testes, utilize o arquivo de importa��o para POSTMAN disponibilizado na raiz do projeto:

DASA TESTE.postman_collection.json

Com ele � poss�vel realizar chamadas prontas em todas as APIs

```

## Console banco de dados em mem�ria

Para acessar o console do H2:

http://localhost:9000/h2
Logim: SA
Password: password

---

REQUISITOS:

```
- Laborat�rio:
  - cadastrar um novo labor�rio;
  - obter uma lista de laborat�rios ativos;
  - atualizar um laborat�rio existente;
  - remover logicamente um laborat�rio ativo.

- Exames:
  - cadastrar um novo exame;
  - obter uma lista de exames ativos;
  - atualizar um exame existente;
  - remover logicamente um exame ativo.

- Associa��o:
  - associar um exame ativo � um laborat�rio ativo;
  - desassociar um exame ativo de um laborat�rio ativo;

  **Importante:**

  - Um exame pode estar associado a mais de um laborat�rio;
  - O cadastro de um laborat�rio/exame � considerado ativo e recebe um `id` gerado automaticamente.
```

## DESCRI��O METODOS DA API:

```
EXAME CONTROLLER

GET
path /exame
Listar exames ativos

POST
path /exame
Gravar um exame

PUT
path /exame
Alterar um exame

DELETE
path /exame
Deletar um exame (Exclus�o l�gica)

GET
path /exame/{id}
Exibir exame por ID

GET
path /exame/filtro
Exibir exame por filtro

POST
path /lote/exame
Gravar exames em lote

PUT
path /lote/exame
Alterar exames em lote

DELETE
path /lote/exame
Deletar exames em lote (Exclus�o l�gica)

```
```
LABORATORIO CONTROLLER

GET
path /laboratorio
Listar laborat�rios ativos

POST 
/laboratorio
Gravar um laborat�rio

PUT
/laboratorio
Alterar um laborat�rio

DELETE
/laboratorio
Deletar um laborat�rio (Exclus�o l�gica)

GET
/laboratorio/{id}
Exibir laborat�rio por ID

GET
/laboratorio/filtro
Exibir laborat�rio por filtro

POST
/lote/laboratorio
Gravar laborat�rio em lote

PUT
/lote/laboratorio
Alterar laborat�rio em lote

DELETE
/lote/laboratorio
Deletar laborat�rios em lote (Exclus�o l�gica)

```

```
LABORATORIO EXAME CONTROLLER

POST
/laboratorioExame
Incluir Relacionamento entre laborat�rio e exame

DELETE
/laboratorioExame
Excluir Relacionamento entre laborat�rio e exame


```

## ESTRUTURA DE DADOS

```
MODELO DE DADOS CRIADO PARA A SOLU��O:

Tabelas:
1. EXAME
2. LABORATORIO
3. LABORATORIO_EXAME

Estrutura conforme a DDL Abaixo:


CREATE TABLE EXAME (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  nome VARCHAR(250) NOT NULL,
  tipo VARCHAR(20) NOT NULL,
  status NUMERIC(1) DEFAULT 1
);

CREATE SEQUENCE SEQ_EXAME
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 999999999999
  START 10
  CACHE 1;
 
INSERT INTO EXAME (nome, tipo) VALUES
  ('Hemograma', 'ANALISE_CLINICA'),
  ('Raio-X', 'IMAGEM'),
  ('Resson�ncia Magn�tica', 'IMAGEM');


CREATE TABLE LABORATORIO (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  nome VARCHAR(250) NOT NULL,
  logradouro VARCHAR(500) NOT NULL,
  numero VARCHAR(50),
  complemento VARCHAR(50),
  cidade VARCHAR(100),
  estado VARCHAR(50),
  cep VARCHAR(10),
  status NUMERIC(1) DEFAULT 1
);

CREATE TABLE LABORATORIO_EXAME (
	id INT AUTO_INCREMENT PRIMARY KEY,
	EXAME_ID INT,
	LABORATORIO_ID INT,
  	foreign key (EXAME_ID) references EXAME(ID),
  	foreign key (LABORATORIO_ID) references LABORATORIO(ID)
);

CREATE SEQUENCE SEQ_LABORATORIO_EXAME
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 999999999999
  START 10
  CACHE 1;

```

