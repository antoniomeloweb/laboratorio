package br.com.dasa.teste.laboratorio.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.com.dasa.teste.laboratorio.domain.Exame;
import br.com.dasa.teste.laboratorio.dto.ExameDTO;
import br.com.dasa.teste.laboratorio.repository.ExameRepository;
import lombok.extern.java.Log;

@Service
@Log
public class ExameService {
	
	@Autowired
	private ExameRepository repository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	public Exame findById(Long id){
		Exame retorno = repository.findById(id).orElse(null);
		
		if(Objects.isNull(retorno)) {
			throw new ResponseStatusException(
			          HttpStatus.NO_CONTENT, "Não há exames cadastrados");
		}
		return retorno;
	}
	
	
	public List<Exame> findByFiltro(ExameDTO dto){
		Exame exame = modelMapper.map(dto, Exame.class);
		
		//somente ativos
		exame.setStatus("1");
		Example<Exame> exameExample = Example.of(exame);
		
		List<Exame> retorno = repository.findAll(exameExample);
		
		if(Objects.isNull(retorno) || retorno.size()==0) {
			throw new ResponseStatusException(
			          HttpStatus.NO_CONTENT, "Não há exames cadastrados");
		}
		return retorno;
	}
	
	public void persistExame(ExameDTO exame) {
		
		Exame persist = modelMapper.map(exame, Exame.class);
		
		try {
			repository.save(persist);
		}catch(Exception ex){
			log.log(Level.SEVERE, "Não foi possível persistir o exame.");
		}
		
	}

	public void updateExame(ExameDTO exame) {
		Exame persist = modelMapper.map(exame, Exame.class);
		
		try {
			Exame e = repository.findById(persist.getId()).orElse(null);
			if(Objects.isNull(e)) {
				throw new ResponseStatusException(HttpStatus.NO_CONTENT);
			}
			repository.save(persist);
		}catch(Exception ex){
			log.log(Level.SEVERE, "Não foi possível alterar o exame id: "+exame.getId());
		}
	
	}
	
	public void deleteExame(ExameDTO exame) {
		Exame persist = modelMapper.map(exame, Exame.class);
		
		//repository.deleteById(exame.getId());
		exclusaoLogica(persist);
	
	}
	
	public List<Exame> findAllAtivos(){
		List<Exame> retorno = new ArrayList<Exame>();
		
		retorno = repository.findAllAtivos();
		
		if(retorno.size()==0) {
			throw new ResponseStatusException(
			          HttpStatus.NO_CONTENT, "Não há exames cadastrados");
		}
		return retorno;
	}
	
	public List<Exame> findAll(){
		List<Exame> retorno = new ArrayList<Exame>();
		
		retorno = repository.findAll();
		
		if(retorno.size()==0) {
			throw new ResponseStatusException(
			          HttpStatus.NO_CONTENT, "Não há exames cadastrados");
		}
		return retorno;
	}
	
	public void persistLoteExame(List<ExameDTO> exame) {
		
		List<Exame> listaPersist = modelMapper.map(exame, new TypeToken<List<Exame>>() {}.getType());
		
		for(Exame e: listaPersist) {
			try {
				repository.save(e);
			}catch(Exception ex){
				log.log(Level.SEVERE, "Não foi possível persistir o exame id: "+e.getId());
			}
		}
		
	}

	public void updateLoteExame(List<ExameDTO> exame) {
		List<Exame> listaPersist = modelMapper.map(exame, new TypeToken<List<Exame>>() {}.getType());
		
		for(Exame e: listaPersist) {
			try {
				repository.save(e);
			}catch(Exception ex){
				log.log(Level.SEVERE, "Não foi possível alterar o exame id: "+e.getId());
			}
		}
	}
	
	public void deleteLoteExame(List<ExameDTO> exame) {
		List<Exame> listaPersist = modelMapper.map(exame, new TypeToken<List<Exame>>() {}.getType());
		
		for(Exame e: listaPersist) {
			try {
				//repository.deleteById(e.getId());
				exclusaoLogica(e);
			}catch(Exception ex){
				log.log(Level.SEVERE, "Não foi possível deletar o exame id: "+e.getId());
			}
		}
	}
	
	public void exclusaoLogica(Exame exame) {
		Exame e = repository.findById(exame.getId()).orElse(null);;
		
		if(Objects.isNull(e)) {
			throw new ResponseStatusException(
			          HttpStatus.NO_CONTENT, "Exame não encontrado.");
		}
		e.setStatus("0");
		repository.save(e);
	}
}
