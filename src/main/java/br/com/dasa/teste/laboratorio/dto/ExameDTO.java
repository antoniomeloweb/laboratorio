package br.com.dasa.teste.laboratorio.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ExameDTO implements Serializable{

	private static final long serialVersionUID = -2949048521273848985L;

	private Long id;
	
	private String status;
	
	private TipoEnum tipo;

	private String nome;
	
	@JsonProperty(value ="listaLaboratorio")
	private Set<LaboratorioPuroDTO> listaLaboratorio = new HashSet<LaboratorioPuroDTO>();
}
