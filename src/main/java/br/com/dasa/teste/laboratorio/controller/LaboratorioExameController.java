package br.com.dasa.teste.laboratorio.controller;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.dasa.teste.laboratorio.api.LaboratorioExameApi;
import br.com.dasa.teste.laboratorio.dto.LaboratorioExameDTO;
import br.com.dasa.teste.laboratorio.service.LaboratorioExameService;
import io.swagger.annotations.ApiOperation;

	@RestController
	public class LaboratorioExameController implements LaboratorioExameApi{

		@Autowired
		private LaboratorioExameService service;
		
		private ModelMapper modelMapper;

		@Override
		@RequestMapping(value  = "/laboratorioExame", method=RequestMethod.POST)
		@ApiOperation(value = "Incluir Relacionamento entre laboratorio e exame")
		public ResponseEntity<?> incluirRelacionamento(@RequestBody LaboratorioExameDTO laboratorioExame) {

			service.inserirRelacionamento(laboratorioExame);
			
			return new ResponseEntity(HttpStatus.OK);
		}

		@Override
		@RequestMapping(value  = "/laboratorioExame", method=RequestMethod.DELETE)
		@ApiOperation(value = "Excluir Relacionamento entre laboratorio e exame")
		public ResponseEntity<?> excluirRelacionamento(@RequestBody LaboratorioExameDTO laboratorioExame) {

			service.deletarRelacionamento(laboratorioExame);
			
			return new ResponseEntity(HttpStatus.OK);
		}

	}