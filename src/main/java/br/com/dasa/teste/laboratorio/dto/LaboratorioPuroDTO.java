package br.com.dasa.teste.laboratorio.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class LaboratorioPuroDTO implements Serializable{

		private static final long serialVersionUID = 2199543478854672110L;

		private Long id;
		
		private String status;
		private String nome;
		private String logradouro;
		private String numero;
		private String complemento;
		private String cidade;
		private String estado;
		private String cep;
		
	}