package br.com.dasa.teste.laboratorio.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;

import lombok.Data;

@Data
@Entity(name="EXAME")
public class Exame implements Serializable{

	public Exame() {
		laboratorioExame = new HashSet<LaboratorioExame>();
	}
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_EXAME")
	@Column(name="id")
	private Long id;
	
	@Column(name="status")
	private String status;
	
	@Column(name="tipo")
	private String tipo;

	@Column(name="nome")
	private String nome;
	
	//@OneToMany(mappedBy = "exameId",cascade = CascadeType.ALL,fetch = FetchType.EAGER,orphanRemoval = true)
	@OneToMany(mappedBy = "exame")
    private Set<LaboratorioExame> laboratorioExame = new HashSet<LaboratorioExame>();
}
