package br.com.dasa.teste.laboratorio.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import lombok.Data;

@Data
@Entity(name="LABORATORIO")
public class Laboratorio implements Serializable{

	private static final long serialVersionUID = 2199543478854672110L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_LABORATORIO")
	@Column(name="id")
	private Long id;
	
	@Column(name="status")
	private String status;
	
	@Column(name="nome")
	private String nome;
	
	@Column(name="logradouro")
	private String logradouro;
	
	@Column(name="numero")
	private String numero;
	
	@Column(name="complemento")
	private String complemento;
	
	@Column(name="cidade")
	private String cidade;
	
	@Column(name="estado")
	private String estado;
	
	@Column(name="cep")
	private String cep;
	
	//@OneToMany(mappedBy = "", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
	@OneToMany(mappedBy = "laboratorio")
    private Set<LaboratorioExame> laboratorioExame = new HashSet<LaboratorioExame>();
}
