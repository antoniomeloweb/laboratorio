package br.com.dasa.teste.laboratorio.dto;

public enum TipoEnum {
	ANALISE_CLINICA("Análise Clínica"),
	IMAGEM("Imagem");
	
	public String nome;
	
	TipoEnum(String nome){
		this.nome = nome;
	}
	
}
