package br.com.dasa.teste.laboratorio.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;

@Entity(name = "LABORATORIO_EXAME")
public class LaboratorioExame implements Serializable {

	private static final long serialVersionUID = -7350017907147721715L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_LABORATORIO_EXAME")
	@Column(name="id")
	private Long id;
	
    @ManyToOne
    //@MapsId("exameId")
    @JoinColumn(name = "exame_id")
    Exame exame;
 
    @ManyToOne
    //@MapsId("laboratorioId")
    @JoinColumn(name = "laboratorio_id")
    Laboratorio laboratorio;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Exame getExame() {
		return exame;
	}

	public void setExame(Exame exame) {
		this.exame = exame;
	}

	public Laboratorio getLaboratorio() {
		return laboratorio;
	}

	public void setLaboratorio(Laboratorio laboratorio) {
		this.laboratorio = laboratorio;
	}

    
}
