package br.com.dasa.teste.laboratorio.service;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.com.dasa.teste.laboratorio.domain.Exame;
import br.com.dasa.teste.laboratorio.domain.Laboratorio;
import br.com.dasa.teste.laboratorio.domain.LaboratorioExame;
import br.com.dasa.teste.laboratorio.dto.LaboratorioExameDTO;
import br.com.dasa.teste.laboratorio.repository.ExameRepository;
import br.com.dasa.teste.laboratorio.repository.LaboratorioExameRepository;
import br.com.dasa.teste.laboratorio.repository.LaboratorioRepository;
import lombok.extern.java.Log;


	@Service
	@Log
	public class LaboratorioExameService {

		@Autowired
		private LaboratorioExameRepository repository;
	
		@Autowired 
		private ExameRepository exameRepository;
		
		@Autowired 
		private LaboratorioRepository laboratorioRepository;
		
		public void inserirRelacionamento(LaboratorioExameDTO laboratorioExame) {
			Long idExame = laboratorioExame.getIdExame();
			Long idLaboratorio = laboratorioExame.getIdLaboratorio();
			
			List<LaboratorioExame> entity = repository.findByExameIdAndLaboratorioId(idExame, idLaboratorio);
			
			if(Objects.isNull(entity) || entity.size()==0) {
				LaboratorioExame le = new LaboratorioExame();
				
				Exame exame = exameRepository.findById(idExame).orElse(null);
				Laboratorio laboratorio = laboratorioRepository.findById(idLaboratorio).orElse(null);
				
				if(Objects.isNull(exame)) {
					throw new ResponseStatusException(
					          HttpStatus.NO_CONTENT, "Exame não encontrado");
				}else if(Objects.isNull(laboratorio)) {
					throw new ResponseStatusException(
					          HttpStatus.NO_CONTENT, "Laboratório não encontrado");
				}else if(!"1".equals(exame.getStatus())) {
					throw new ResponseStatusException(
					          HttpStatus.NO_CONTENT, "Exame não está ativo");
				}else if(!"1".equals(laboratorio.getStatus())) {
					throw new ResponseStatusException(
					          HttpStatus.NO_CONTENT, "Laboratorio não está ativo");
				}
				le.setExame(exame);
				le.setLaboratorio(laboratorio);
				
				repository.save(le);
			}else {
				throw new ResponseStatusException(
				          HttpStatus.CONFLICT, "associação já existe");				
			}
		}
		
		public void deletarRelacionamento(LaboratorioExameDTO laboratorioExame) {
			Long idExame = laboratorioExame.getIdExame();
			Long idLaboratorio = laboratorioExame.getIdLaboratorio();
			
			List<LaboratorioExame> entity = repository.findByExameIdAndLaboratorioId(idExame, idLaboratorio);
			
			if(!Objects.isNull(entity) &&  entity.size()>0) {
				for(LaboratorioExame l: entity) {
					Exame exame = exameRepository.findById(idExame).orElse(null);
					Laboratorio laboratorio = laboratorioRepository.findById(idLaboratorio).orElse(null);
					
					if(!"1".equals(exame.getStatus())) {
							throw new ResponseStatusException(
							          HttpStatus.NO_CONTENT, "Exame não está ativo");
					}else if(!"1".equals(laboratorio.getStatus())) {
						throw new ResponseStatusException(
						          HttpStatus.NO_CONTENT, "Laboratorio não está ativo");
					}
					repository.delete(l);
				}
			}else {
				throw new ResponseStatusException(
				          HttpStatus.NO_CONTENT, "associação não encontrada");				
			}
		}
	}