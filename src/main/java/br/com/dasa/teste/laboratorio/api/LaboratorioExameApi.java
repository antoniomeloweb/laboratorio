package br.com.dasa.teste.laboratorio.api;

import org.springframework.http.ResponseEntity;

import br.com.dasa.teste.laboratorio.dto.LaboratorioExameDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "/laboratorioExame", produces = "application/json")
@ApiOperation(value = "value", notes = "notes")
public interface LaboratorioExameApi {

	public ResponseEntity<?> incluirRelacionamento(LaboratorioExameDTO laboratorioExame);

	public ResponseEntity<?> excluirRelacionamento(LaboratorioExameDTO laboratorioExame);
	
	//public ResponseEntity<?> incluirRelacionamentoLote(List<LaboratorioExame> laboratorioExame);

	//public ResponseEntity<?> excluirRelacionamentoLote(List<LaboratorioExame> laboratorioExame);
}
