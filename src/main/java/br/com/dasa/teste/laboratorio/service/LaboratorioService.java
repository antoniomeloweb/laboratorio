package br.com.dasa.teste.laboratorio.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.com.dasa.teste.laboratorio.domain.Exame;
import br.com.dasa.teste.laboratorio.domain.Laboratorio;
import br.com.dasa.teste.laboratorio.dto.ExameDTO;
import br.com.dasa.teste.laboratorio.dto.LaboratorioDTO;
import br.com.dasa.teste.laboratorio.repository.LaboratorioRepository;
import lombok.extern.java.Log;

@Service
@Log
public class LaboratorioService {

	@Autowired
	private LaboratorioRepository repository;

	@Autowired
	private ModelMapper modelMapper;

	public List<Laboratorio> findByFiltro(LaboratorioDTO dto){
		Laboratorio lab= modelMapper.map(dto, Laboratorio.class);
		
		//somente ativos
		lab.setStatus("1");
		Example<Laboratorio> exameExample = Example.of(lab);
		
		List<Laboratorio> retorno = repository.findAll(exameExample);
		
		if(Objects.isNull(retorno) || retorno.size()==0) {
			throw new ResponseStatusException(
			          HttpStatus.NO_CONTENT, "Não há laboratorios cadastrados");
		}
		return retorno;
	}
	
	public Laboratorio findById(Long id){
		Laboratorio retorno = repository.findById(id).orElse(null);;
		
		if(Objects.isNull(retorno)) {
			throw new ResponseStatusException(
			          HttpStatus.NO_CONTENT, "Laboratório não encontrado");
		}
		return retorno;
	}
	
	public List<Laboratorio> findAllAtivos(){
		List<Laboratorio> retorno = new ArrayList<Laboratorio>();
		
		retorno = repository.findAllAtivos();
		
		if(retorno.size()==0) {
			throw new ResponseStatusException(
			          HttpStatus.NO_CONTENT, "Não há laboratorios cadastrados");
		}
		return retorno;
	}
	
	public List<Laboratorio> findAll() {
		List<Laboratorio> retorno = new ArrayList<Laboratorio>();

		retorno = repository.findAll();

		if (retorno.size() == 0) {
			throw new ResponseStatusException(HttpStatus.NO_CONTENT, "Não há laboratórios cadastrados");
		}
		return retorno;
	}

	public void persistExame(LaboratorioDTO laboratorio) {

		Laboratorio persist = modelMapper.map(laboratorio, Laboratorio.class);

		try {
			repository.save(persist);
		} catch (Exception ex) {
			log.log(Level.SEVERE, "Não foi possível persistir o Laboratorio id: " + persist.getId());
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
		}
		
	}

	public void updateExame(LaboratorioDTO laboratorio) {
		Laboratorio persist = modelMapper.map(laboratorio, Laboratorio.class);

		try {
			Laboratorio l = repository.findById(persist.getId()).get();
			if(Objects.isNull(l)) {
				throw new ResponseStatusException(HttpStatus.NO_CONTENT);
			}
			repository.save(persist);
		} catch (Exception ex) {
			log.log(Level.SEVERE, "Não foi possível alterar o Laboratorio id: " + persist.getId());
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
		}
	}

	public void deleteExame(LaboratorioDTO laboratorio) {
		Laboratorio persist = modelMapper.map(laboratorio, Laboratorio.class);

		//repository.deleteById(l.getId());
		exclusaoLogica(persist);

	}
	
	public void persistLoteExame(List<LaboratorioDTO> laboratorio) {

		List<Laboratorio> listaPersist = modelMapper.map(laboratorio, new TypeToken<List<Laboratorio>>() {
		}.getType());

		for (Laboratorio l : listaPersist) {
			try {
				repository.save(l);
			} catch (Exception ex) {
				log.log(Level.SEVERE, "Não foi possível persistir o Laboratorio id: " + l.getId());
			}
		}

	}

	public void updateLoteExame(List<LaboratorioDTO> laboratorio) {
		List<Laboratorio> listaPersist = modelMapper.map(laboratorio, new TypeToken<List<Laboratorio>>() {
		}.getType());

		for (Laboratorio l : listaPersist) {
			try {
				repository.save(l);
			} catch (Exception ex) {
				log.log(Level.SEVERE, "Não foi possível alterar o Laboratorio id: " + l.getId());
			}
		}
	}

	public void deleteLoteExame(List<LaboratorioDTO> laboratorio) {
		List<Laboratorio> listaPersist = modelMapper.map(laboratorio, new TypeToken<List<Laboratorio>>() {
		}.getType());

		for (Laboratorio l : listaPersist) {
			try {
				//repository.deleteById(l.getId());
				exclusaoLogica(l);
			} catch (Exception ex) {
				log.log(Level.SEVERE, "Não foi possível deletar o Laboratorio id: " + l.getId());
			}
		}
	}	
	
	
	public void exclusaoLogica(Laboratorio laboratorio) {
		Laboratorio l = repository.findById(laboratorio.getId()).orElse(null);;
		
		if(Objects.isNull(l)) {
			throw new ResponseStatusException(
			          HttpStatus.NO_CONTENT, "Laboratorio não encontrado.");
		}
		l.setStatus("0");
		repository.save(l);
	}
}