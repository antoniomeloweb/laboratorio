package br.com.dasa.teste.laboratorio.repository;

import java.util.List;

import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import br.com.dasa.teste.laboratorio.domain.Exame;
import br.com.dasa.teste.laboratorio.domain.Laboratorio;

public interface LaboratorioRepository extends CrudRepository<Laboratorio, Long> {
	
	@Override
	public List<Laboratorio> findAll();
	
	@Query(value = "SELECT * FROM LABORATORIO l WHERE l.status = 1", nativeQuery = true)
	public List<Laboratorio> findAllAtivos();
	
	public List<Laboratorio> findAll(Example<Laboratorio> exame);
}