package br.com.dasa.teste.laboratorio.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class ExamePuroDTO implements Serializable{

	private static final long serialVersionUID = -2949048521273848985L;

	private Long id;
	
	private String status;
	
	private TipoEnum tipo;

	private String nome;
	
}
