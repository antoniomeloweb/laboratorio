package br.com.dasa.teste.laboratorio.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import br.com.dasa.teste.laboratorio.domain.LaboratorioExame;

public interface LaboratorioExameRepository extends CrudRepository<LaboratorioExame, Long> {

	@Query(value = "SELECT * FROM LABORATORIO_EXAME l WHERE l.laboratorio_id = ?1", nativeQuery = true)
	public List<LaboratorioExame> findAllByLaboratorioId(Long idLaboratorio);


	@Query(value = "SELECT * FROM LABORATORIO_EXAME l WHERE l.exame_id = ?1", nativeQuery = true)
	public List<LaboratorioExame> findAllByExameId(Long idExame);
	
	@Query(value = "SELECT * FROM LABORATORIO_EXAME l WHERE l.exame_id = ?1 and l.laboratorio_id = ?2", nativeQuery = true)
	public List<LaboratorioExame> findByExameIdAndLaboratorioId(Long idExame, Long idLaboratorio);
}