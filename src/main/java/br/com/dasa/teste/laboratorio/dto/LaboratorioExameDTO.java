package br.com.dasa.teste.laboratorio.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class LaboratorioExameDTO implements Serializable {

	private static final long serialVersionUID = 2199543478854672110L;

	private Long idExame;

	private Long idLaboratorio;
}