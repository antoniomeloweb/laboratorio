package br.com.dasa.teste.laboratorio.api;

import java.util.List;

import org.springframework.http.ResponseEntity;

import br.com.dasa.teste.laboratorio.dto.ExameDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "/exame", produces = "application/json")
@ApiOperation(value = "value", notes = "notes")
public interface ExameApi {
	public ResponseEntity<ExameDTO> getExameById(Long id);
	public ResponseEntity<List<ExameDTO>> getExameByFiltro(ExameDTO exame);
	
	public ResponseEntity<List<ExameDTO>> getListExame();
	
	public ResponseEntity<?> persistExame(ExameDTO exames);
	public ResponseEntity<?> updateExame(ExameDTO exames);
	public ResponseEntity<?> deleteExame(ExameDTO exames);
	
	public ResponseEntity<?> persistLoteExame(List<ExameDTO> exames);
	public ResponseEntity<?> updateLoteExame(List<ExameDTO> exames);
	public ResponseEntity<?> deleteLoteExame(List<ExameDTO> exames);
}
