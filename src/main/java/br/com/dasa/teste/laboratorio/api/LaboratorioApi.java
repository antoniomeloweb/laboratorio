package br.com.dasa.teste.laboratorio.api;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

import br.com.dasa.teste.laboratorio.dto.LaboratorioDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

	@Api(value = "/laboratorio", produces = "application/json")
	@ApiOperation(value = "value", notes = "notes")
	public interface LaboratorioApi  {
		public ResponseEntity<List<LaboratorioDTO>> getLaboratorioByFiltro(@RequestBody LaboratorioDTO laboratorio) ;
		public ResponseEntity<LaboratorioDTO> getLaboratorioById(Long id);
		
		public ResponseEntity<List<LaboratorioDTO>> getLaboratorio();

		public ResponseEntity<?> persistLaboratorio(LaboratorioDTO laboratorios);
		public ResponseEntity<?> updateLaboratorio(LaboratorioDTO laboratorios);
		public ResponseEntity<?> deleteLaboratorio(LaboratorioDTO laboratorios);
		
		public ResponseEntity<?> persistLoteLaboratorio(List<LaboratorioDTO> laboratorios);
		public ResponseEntity<?> updateLoteLaboratorio(List<LaboratorioDTO> laboratorios);
		public ResponseEntity<?> deleteLoteLaboratorio(List<LaboratorioDTO> laboratorios);
	}