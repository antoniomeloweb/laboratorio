package br.com.dasa.teste.laboratorio.controller;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.dasa.teste.laboratorio.api.ExameApi;
import br.com.dasa.teste.laboratorio.domain.Exame;
import br.com.dasa.teste.laboratorio.domain.LaboratorioExame;
import br.com.dasa.teste.laboratorio.dto.ExameDTO;
import br.com.dasa.teste.laboratorio.dto.LaboratorioDTO;
import br.com.dasa.teste.laboratorio.dto.LaboratorioPuroDTO;
import br.com.dasa.teste.laboratorio.service.ExameService;
import io.swagger.annotations.ApiOperation;

@RestController
public class ExameController implements ExameApi{

	@Autowired
	private ExameService service;
	
	private ModelMapper modelMapper;
	
	public ExameController() {
		PropertyMap<ExameDTO, LaboratorioDTO> clientPropertyMap = new PropertyMap<ExameDTO, LaboratorioDTO>() {
		    @Override
		    protected void configure() {
		        skip(destination.getListaExames());
		    }
		};
		modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setAmbiguityIgnored(true);
		modelMapper.addMappings(clientPropertyMap);

	}
	
	@Override
	@RequestMapping(value  = "/exame", method=RequestMethod.GET)
	@ApiOperation(value = "Listar exames ativos")
	public ResponseEntity<List<ExameDTO>> getListExame() {

		List<Exame> consulta = service.findAllAtivos();

		List<ExameDTO> retorno = modelMapper.map(consulta,  new TypeToken<List<ExameDTO>>() {}.getType());
		return new ResponseEntity<List<ExameDTO>>(retorno, HttpStatus.OK);
	}
	
	@Override
	@RequestMapping(value  = "/exame/filtro", method=RequestMethod.GET)
	@ApiOperation(value = "Exibir exame por filtro")
	public ResponseEntity<List<ExameDTO>> getExameByFiltro(@RequestBody ExameDTO exame) {

		List<Exame> consulta = service.findByFiltro(exame);
		List<ExameDTO> retorno = new ArrayList<ExameDTO>();
		
		for(Exame e: consulta) {
			ExameDTO dto = modelMapper.map(e,  ExameDTO.class);
			List<LaboratorioPuroDTO> labs = getAllLabs(e);
		
			dto.getListaLaboratorio().addAll(labs);
			
			retorno.add(dto);
		}
		
		return new ResponseEntity<List<ExameDTO>>(retorno, HttpStatus.OK);
	}

	@Override
	@RequestMapping(value  = "/exame/{id}", method=RequestMethod.GET)
	@ApiOperation(value = "Exibir exame por ID")
	public ResponseEntity<ExameDTO> getExameById(@PathVariable("id") Long id) {

		Exame consulta = service.findById(id);

		ExameDTO retorno = modelMapper.map(consulta,  ExameDTO.class);
		List<LaboratorioPuroDTO> labs = getAllLabs(consulta);
		
		retorno.getListaLaboratorio().addAll(labs);
		return new ResponseEntity<ExameDTO>(retorno, HttpStatus.OK);
	}
	
	@Override
	@RequestMapping(value = "/exame", method=RequestMethod.POST)
	@ApiOperation(value = "Gravar um exame")
	public ResponseEntity<?> persistExame( @RequestBody ExameDTO exame){
		
		service.persistExame(exame);
		
		return new ResponseEntity(HttpStatus.CREATED);
	}

	@Override
	@RequestMapping(value = "/exame", method=RequestMethod.PUT)
	@ApiOperation(value = "Alterar um exame")
	public ResponseEntity<?> updateExame( @RequestBody ExameDTO exames) {
		service.updateExame(exames);
		
		return new ResponseEntity(HttpStatus.OK);

	}

	@Override
	@RequestMapping(value = "/exame", method=RequestMethod.DELETE)
	@ApiOperation(value = "Deletar um exame (exclusão lógica)")
	public ResponseEntity<?> deleteExame( @RequestBody ExameDTO exames) {
		service.deleteExame(exames);
		return new ResponseEntity(HttpStatus.OK);
	}

	@Override
	@RequestMapping(value = "/lote/exame", method=RequestMethod.POST)
	@ApiOperation(value = "Gravar exames em lote")
	public ResponseEntity<?> persistLoteExame( @RequestBody List<ExameDTO> exame){
		
		service.persistLoteExame(exame);
		
		return new ResponseEntity(HttpStatus.CREATED);
	}

	@Override
	@RequestMapping(value = "/lote/exame", method=RequestMethod.PUT)
	@ApiOperation(value = "Alterar exames em lote")
	public ResponseEntity<?> updateLoteExame( @RequestBody List<ExameDTO> exames) {
		service.updateLoteExame(exames);
		
		return new ResponseEntity(HttpStatus.OK);

	}

	@Override
	@RequestMapping(value = "/lote/exame", method=RequestMethod.DELETE)
	@ApiOperation(value = "Deletar exames em lote (Exclusão lógica)")
	public ResponseEntity<?> deleteLoteExame( @RequestBody List<ExameDTO> exames) {
		service.deleteLoteExame(exames);
		return new ResponseEntity(HttpStatus.OK);
	}

	private List<LaboratorioPuroDTO> getAllLabs(Exame consulta){
		List<LaboratorioPuroDTO> retorno = new ArrayList<LaboratorioPuroDTO>();
		for(LaboratorioExame l: consulta.getLaboratorioExame()) {
			LaboratorioPuroDTO lab = modelMapper.map(l.getLaboratorio(), LaboratorioPuroDTO.class);
			retorno.add(lab);
		}
		return retorno;
	}
}
