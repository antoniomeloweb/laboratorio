package br.com.dasa.teste.laboratorio.controller;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.dasa.teste.laboratorio.api.LaboratorioApi;
import br.com.dasa.teste.laboratorio.domain.Laboratorio;
import br.com.dasa.teste.laboratorio.domain.LaboratorioExame;
import br.com.dasa.teste.laboratorio.dto.ExameDTO;
import br.com.dasa.teste.laboratorio.dto.ExamePuroDTO;
import br.com.dasa.teste.laboratorio.dto.LaboratorioDTO;
import br.com.dasa.teste.laboratorio.service.LaboratorioService;
import io.swagger.annotations.ApiOperation;

	@RestController
	public class LaboratorioController implements LaboratorioApi{

		@Autowired
		private LaboratorioService service;
		
		private ModelMapper modelMapper;
		
		public LaboratorioController() {
			PropertyMap<LaboratorioDTO, ExameDTO> clientPropertyMap = new PropertyMap<LaboratorioDTO, ExameDTO>() {
			    @Override
			    protected void configure() {
			        skip(destination.getListaLaboratorio());
			    }
			};
			modelMapper = new ModelMapper();
			modelMapper.getConfiguration().setAmbiguityIgnored(true);
			modelMapper.addMappings(clientPropertyMap);

		}
		
		@Override
		@RequestMapping(value  = "/laboratorio/filtro", method=RequestMethod.GET)
		@ApiOperation(value = "Exibir laboratorio por filtro")
		public ResponseEntity<List<LaboratorioDTO>> getLaboratorioByFiltro(@RequestBody LaboratorioDTO laboratorio) {

			List<Laboratorio> consulta = service.findByFiltro(laboratorio);
			List<LaboratorioDTO> retorno = new ArrayList<LaboratorioDTO>();
			
			for(Laboratorio e: consulta) {
				LaboratorioDTO dto = modelMapper.map(e, LaboratorioDTO.class);
				List<ExamePuroDTO> labs = getAllExames(e);
			
				dto.getListaExames().addAll(labs);
				
				retorno.add(dto);
			}
			
			return new ResponseEntity<List<LaboratorioDTO>>(retorno, HttpStatus.OK);
		}
		
		@Override
		@RequestMapping(value  = "/laboratorio", method=RequestMethod.GET)
		@ApiOperation(value = "Listar laboratórios ativos")
		public ResponseEntity<List<LaboratorioDTO>> getLaboratorio() {

			List<Laboratorio> consulta = service.findAllAtivos();
			
			List<LaboratorioDTO> retorno = modelMapper.map(consulta,  new TypeToken<List<LaboratorioDTO>>() {}.getType());
			return new ResponseEntity<List<LaboratorioDTO>>(retorno, HttpStatus.OK);
		}
		
		@Override
		@RequestMapping(value  = "/laboratorio/{id}", method=RequestMethod.GET)
		@ApiOperation(value = "Exibir laboratório por ID")
		public ResponseEntity<LaboratorioDTO> getLaboratorioById(@PathVariable("id") Long id) {

			Laboratorio consulta = service.findById(id);
			
			LaboratorioDTO retorno = modelMapper.map(consulta,  LaboratorioDTO.class);
			
			List<ExamePuroDTO> exames = getAllExames(consulta);
			
			retorno.getListaExames().addAll(exames);
			return new ResponseEntity<LaboratorioDTO>(retorno, HttpStatus.OK);
		}

		@Override
		@RequestMapping(value = "/laboratorio", method=RequestMethod.POST)
		@ApiOperation(value = "Gravar um laboratório")
		public ResponseEntity<?> persistLaboratorio( @RequestBody LaboratorioDTO laboratorio){
			
			service.persistExame(laboratorio);
			
			return new ResponseEntity(HttpStatus.CREATED);
		}

		@Override
		@RequestMapping(value = "/laboratorio", method=RequestMethod.PUT)
		@ApiOperation(value = "Alterar um laboratório")
		public ResponseEntity<?> updateLaboratorio( @RequestBody LaboratorioDTO laboratorio) {
			service.updateExame(laboratorio);
			
			return new ResponseEntity(HttpStatus.OK);

		}

		@Override
		@RequestMapping(value = "/laboratorio", method=RequestMethod.DELETE)
		@ApiOperation(value = "Deletar um laboratório (Exclusão Lógica)")
		public ResponseEntity<?> deleteLaboratorio( @RequestBody LaboratorioDTO laboratorio) {
			service.deleteExame(laboratorio);
			return new ResponseEntity(HttpStatus.OK);
		}

		@Override
		@RequestMapping(value = "/lote/laboratorio", method=RequestMethod.POST)
		@ApiOperation(value = "Gravar laboratório em lote")
		public ResponseEntity<?> persistLoteLaboratorio( @RequestBody List<LaboratorioDTO> laboratorio){
			
			service.persistLoteExame(laboratorio);
			
			return new ResponseEntity(HttpStatus.CREATED);
		}

		@Override
		@RequestMapping(value = "/lote/laboratorio", method=RequestMethod.PUT)
		@ApiOperation(value = "Alterar laboratório em lote")
		public ResponseEntity<?> updateLoteLaboratorio( @RequestBody List<LaboratorioDTO> laboratorio) {
			service.updateLoteExame(laboratorio);
			
			return new ResponseEntity(HttpStatus.OK);

		}

		@Override
		@RequestMapping(value = "/lote/laboratorio", method=RequestMethod.DELETE)
		@ApiOperation(value = "Deletar laboratórios em lote (Exclusão Lógica)")
		public ResponseEntity<?> deleteLoteLaboratorio( @RequestBody List<LaboratorioDTO> laboratorio) {
			service.deleteLoteExame(laboratorio);
			return new ResponseEntity(HttpStatus.OK);
		}


		private List<ExamePuroDTO> getAllExames(Laboratorio consulta){
			List<ExamePuroDTO> retorno = new ArrayList<ExamePuroDTO>();
			for(LaboratorioExame l: consulta.getLaboratorioExame()) {
				ExamePuroDTO e = modelMapper.map(l.getExame(), ExamePuroDTO.class);
				retorno.add(e);
			}
			return retorno;
		}
	}

