package br.com.dasa.teste.laboratorio.repository;

import java.util.List;

import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import br.com.dasa.teste.laboratorio.domain.Exame;

public interface ExameRepository extends CrudRepository<Exame, Long> {
	
	@Override
	public List<Exame> findAll();
	
	@Query(value = "SELECT * FROM EXAME l WHERE l.status = 1", nativeQuery = true)
	public List<Exame> findAllAtivos();
	
	public List<Exame> findAll(Example<Exame> exame);
}